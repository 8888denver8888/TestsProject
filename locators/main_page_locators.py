from selenium.webdriver.common.by import By


class HeaderPageLocators:
    LOGO = (By.CSS_SELECTOR, "[class=header__link-logo]")

    CASEC = (By.CSS_SELECTOR, ".header-list__link[href='/kejsy']")

    SERVICES = (By.CSS_SELECTOR, ".header-list__link[href='/uslugi']")

    BLOG = (By.CSS_SELECTOR, ".header-list__link[href='/blog']")

    EVENTS = (By.CSS_SELECTOR, ".header-list__link[href='/sobytiya']")

    WRITE = (By.CSS_SELECTOR, ".header__btn")

    MENU = (By.CSS_SELECTOR, ".header-menu ")

    MENU_ACTIVE = (By.CSS_SELECTOR, ".js-menu")


class BannerPageLocators:
    BANNER_NAME = (By.CSS_SELECTOR, "[class='title title--h1 banner__title']")

    BANNER_SUBTITLE = (By.CSS_SELECTOR, ".banner__subtitle-link")


class CasecSectionPageLocators:
    MORE_CASEC = (By.CSS_SELECTOR, ".more[href='/kejsy']")
