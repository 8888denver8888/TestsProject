from selenium.webdriver.common.by import By

from locators.main_page_locators import *
from pages.base_page import BasePage


class Header(BasePage):
    locators = HeaderPageLocators()

    def current_url(self):
        return self.driver.current_url

    def click_logo(self):
        self.element_is_clickable(self.locators.LOGO).click()

    def click_cases(self):
        self.element_is_clickable(self.locators.CASEC).click()

    def click_services(self):
        self.element_is_clickable(self.locators.SERVICES).click()

    def click_blog(self):
        self.element_is_clickable(self.locators.BLOG).click()

    def click_events(self):
        self.element_is_clickable(self.locators.EVENTS).click()

    def click_write(self):
        self.element_is_clickable(self.locators.WRITE).click()

    def click_menu(self):
        self.element_is_clickable(self.locators.MENU).click()

    def menu_display(self):
        menu = self.element_is_visible(self.locators.MENU_ACTIVE)
        is_menu_open = 'active' in menu.get_attribute('class')
        return is_menu_open


class Banner(BasePage):
    locators = BannerPageLocators()

    def current_url(self):
        return self.driver.current_url

    def switch_to_second_window(self):
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[1])

    def check_banner_name(self):
        banner_name = self.element_is_visible(self.locators.BANNER_NAME).text
        return banner_name

    def check_banner_subtitle(self):
        self.element_is_clickable(self.locators.BANNER_SUBTITLE).click()


class CasecSection(BasePage):
    locators = CasecSectionPageLocators()

    def current_url(self):
        return self.driver.current_url

    def click_more_casec(self):
        self.element_is_clickable(self.locators.MORE_CASEC).click()
