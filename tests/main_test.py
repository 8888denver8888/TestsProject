import time

from pages.main_page import *
from conftest import driver


class TestMain:
    class TestHeader:

        def test_logo(self, driver):
            test_logo = Header(driver, "")
            test_logo.open()
            test_logo.click_logo()
            assert test_logo.current_url() == "https://garpix.com/", "Url does not match expected"

        def test_header_list_cases(self, driver):
            test_casec = Header(driver, "")
            test_casec.open()
            test_casec.click_cases()
            assert test_casec.current_url() == "https://garpix.com/kejsy", "Url does not match expected"

        def test_header_list_services(self, driver):
            test_services = Header(driver, "")
            test_services.open()
            test_services.click_services()
            assert test_services.current_url() == "https://garpix.com/uslugi", "Url does not match expected"

        def test_header_list_blogs(self, driver):
            test_blog = Header(driver, "")
            test_blog.open()
            test_blog.click_blog()
            assert test_blog.current_url() == "https://garpix.com/blog", "Url does not match expected"

        def test_header_list_events(self, driver):
            test_events = Header(driver, "")
            test_events.open()
            test_events.click_events()
            assert test_events.current_url() == "https://garpix.com/sobytiya", "Url does not match expected"

        def test_header_list_write(self, driver):
            test_write = Header(driver, "")
            test_write.open()
            test_write.click_write()
            assert test_write.current_url() == "https://garpix.com/feedback?front_key=start_project", "Url does not match expected"

        def test_header_list_menu(self, driver):
            test_menu = Header(driver, "")
            test_menu.open()
            test_menu.click_menu()
            assert test_menu.menu_display() == True, "Menu is not displayed"

    class TestBanner:

        def test_banner_name(self, driver):
            test_banner_name = Banner(driver, "")
            test_banner_name.open()
            banner_name = test_banner_name.check_banner_name()
            assert banner_name == "Digital-интегратор\n№1", "Banner name does not match expected"

        def test_banner_subtitle(self, driver):
            test_banner_subtitle = Banner(driver, "")
            test_banner_subtitle.open()
            test_banner_subtitle.check_banner_subtitle()
            test_banner_subtitle.switch_to_second_window()
            assert test_banner_subtitle.current_url() == "https://ratingruneta.ru/web/dgango-cms/high/", "Url does not match expected"

    class TestCasecSection:

        def test_more_casec(self, driver):
            test_casec_section = CasecSection(driver, "")
            test_casec_section.open()
            test_casec_section.click_more_casec()
            assert test_casec_section.current_url() == "https://garpix.com/kejsy", "Url does not match expected"
